+++
title = "Jade's mavenized"
myDate = "2021-17-08"
description = "Maven repository"
+++

Hi all !

Jade 4.5.3 is now available through maven. You can thus directly integrate it within your projects as a dependency.  See [the download page](https://jade-project.gitlab.io/page/download/)

Here is what we plan for now:

### Communication/Community
- [ ] Launch a survey on what the users expect to see in Jade's next core-releases
- [ ] Invite researchers/devs/teachers/students to joint us
- [ ] Create a Jade Twitter account
- [ ] Develop online courses on Jade
- [x] Develop the start-jade project so that tutorials and available examples on how to use jade grow

### Code 
- [x] Intend to import the full jade-svn-history on the git repo 
- [x] Offer maven import through jitpack 
- [ ] Plan an annual release + nightly builds through CI
- [ ] Make use of Java' last functionnalitities 
- [ ] Offer a tools lib for commonly needed but not core functionalities (service registration,..)
- [ ] Improve code reusability and agent's ability sharing to better benefit of community developments.
- [ ] Add and prioritise functionalities according to the community insights

The survey should be launched by the end of september 2021.

All contribution are welcome, join us [on discord](https://discord.gg/m9DgNkU) !
