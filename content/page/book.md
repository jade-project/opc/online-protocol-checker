---
title: JADE Book
subtitle: Developing Multi-Agent Systems with JADE
comments: false
---

{{< figure link="/img/JadeCover.jpg">}}

[Developing Multi-Agent Systems with JADE;
Fabio Luigi Bellifemine, Giovanni Caire, Dominic Greenwood
© 2007 John Wiley & Sons, Ltd](https://www.wiley.com/en-gb/Developing+Multi+Agent+Systems+with+JADE-p-9780470057476)




