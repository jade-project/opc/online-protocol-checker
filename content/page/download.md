---
title: Download Jade

comments: false
---


## Jade's fork, aka Jade 4.5.3 can now be directly integrated with Maven

### Repository to add to your pom

	<repositories>
		<repository>
		    <id>jitpack.io</id>
		    <url>https://jitpack.io</url>
		</repository>
	</repositories>

### Dependency to add to your pom
	<dependency>
	    <groupId>com.gitlab.jade-project</groupId>
	    <artifactId>jade</artifactId>
	    <version>master-SNAPSHOT</version>
	</dependency>

## Jade's fork repository

[https://gitlab.com/jade-project](https://gitlab.com/jade-project)


## JADE 4.5 is still available for download :

* [Jade-bin-4.5.0](https://gitlab.com/jade-project/jade/-/blob/master/archive/Jade-all-4.5.0/JADE-bin-4.5.0.zip)
* [Jade-doc-4.5.0](https://gitlab.com/jade-project/jade/-/blob/master/archive/Jade-all-4.5.0/JADE-doc-4.5.0.zip)
* [Jade-examples-4.5.0](https://gitlab.com/jade-project/jade/-/blob/master/archive/Jade-all-4.5.0/JADE-examples-4.5.0.zip)
* [Jade-src-4.5.0](https://gitlab.com/jade-project/jade/-/blob/master/archive/Jade-all-4.5.0/JADE-src-4.5.0.zip)


