---
title: Guides and Tutorials

comments: false
---

## Basic documentation

 * [Jade Administrator's Guide](/docs/administratorsguide.pdf) and [Jade Administration Tutorial](/page/JADEAdmin/index.html)
 * [Jade Programmer's Guide ](/docs/programmersguide.pdf) and [Jade Programming Tutorial for beginners](/docs/JADEProgramming-Tutorial-for-beginners.pdf)
 
 * [A Methodology for the Analysis and Design of Multi-Agent Systems using JADE](/docs/JADE_methodology_website_version.pdf)
 
 * [Jade languages and ontologies support](/docs/CLOntoSupport.pdf) 
 * [Jade Tutorial on the creating ontologies by means of the bean-ontology class](/docs/BeanOntologyTutorial.pdf)
 * [API](API/index.html)

## Additional Guides and Tutorials

  * [Introduction to JADE](/docs/Jade-multiagent-platform_Principles-and-main-functionalities-Herpson-2022.pdf) and [basic examples](https://startjade.gitlab.io/)
  * [Teaching resources and examples (in French)](https://emmanueladam.github.io/jade/)
  * [Jade tutorial in Portuguese](/docs/ManualJadePortuguese.pdf)

  * [FIPA and ACL standards](/docs/JADETutorial_FIPA.pdf)
  * [Running jade on Android devices](/docs/JadeAndroid-Programming-Tutorial.pdf)

## Add-ons documentation

 * [Jade Web-Services Integration Gateway (WSIG) ](/docs/add-on/WSIG_Guide.pdf)
 * [Jade Web Services Dynamic Client (WSDC)](/docs/add-on/Jade_WSDC_Guide.pdf)
 * [Jade Gateway tutorial](/docs/add-on/JadeGateway.pdf)

 * [Jade test Suite](/docs/add-on/JADE_TestSuite.pdf)
 * [Jade OSGi ](/docs/add-on/JadeOsgi_Guide.pdf)

 * [Jade Security (Jade-S) ](/docs/add-on/JADE_Security.pdf)
 * [Jade public key infrastructure (PKI) ](/docs/add-on/PKI-Guide.pdf)
 * [Jade Semantics Add-on Programmer's ](docs/add-on/SemanticsProgrammerGuide.pdf)
 

  More to come.

